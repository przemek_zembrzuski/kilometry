(()=>{
        document.body.addEventListener('click',(e)=>{
            if(e.target.id == 'submit' && e.target.nodeName == 'BUTTON'){
                const infoData = document.querySelector('input[name="address"]').dataset;
                if(infoData.destinations_address && infoData.destinations_distance){
                    infoMenu(infoData)
                }else{
                    return
                }
                
            }else if(e.target.id == 'back' && e.target.nodeName == 'BUTTON'){
                    purgeContent(search)
                    const html = ` <input id="locationTextField" type="text" placeholder="Adres" name='address'>
                                    <button id="submit">Szukaj</button>
                                `;
                    search.insertAdjacentHTML('afterbegin',html);
                    search.classList.remove('result');
                    search.classList.add('search');
                    initMap();
            }
        })

        const search = document.querySelector('.search');
        const purgeContent = (parent)=>{
            while(parent.firstChild){
                parent.removeChild(parent.firstChild)
            }
        }
        const infoMenu = (data)=>{
            purgeContent(search);
            search.classList.remove('search');
            search.classList.add('result');
            let html = `
                        <div>
                            <p>ul. ${data.destinations_address}</p>
                            <p>Odległość od Belfiore: ${((data.destinations_distance)/1000).toFixed(1)}km</p>
                            <p>Cena za dowóz: ${calculatePrice(data.destinations_distance)}</p>
                        </div>
                        <button id='back'>Cofnij</button>
                        `
            setTimeout(()=>{search.insertAdjacentHTML('afterbegin',html)},400);
        }

        const calculatePrice = (distance)=>{
            const prices = {
                    25000:12,
                    10000:6,
                    5000:4,
                    3000:2
                    
            }
            let minValues = [];
                Object.keys(prices).map((key)=>{
                    if(distance <= parseInt(key)){
                        minValues.push(prices[key])
                    }
                });
            return minValues.length != 0 ? `${minValues.reduce((a,b)=>Math.min(a,b))}zł` : "Bład wyniku spróbuj jeszcze raz"     
        }
})()
